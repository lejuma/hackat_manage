<?php
include_once "bd.inc.php";

if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}
require_once("$root/model/Project.php");

// TODO A completer

function getProjects() : array {

    // TODO Connecter à la BDD

    $resultat = array();

    $resultat[] = new Project("Premier Projet");
    $resultat[] = new Project("Second Projet");

    return $resultat;
}

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    // prog principal de test
    header('Content-Type:text/plain');

    echo "getProjects() : \n";
    print_r(getProjects());
}