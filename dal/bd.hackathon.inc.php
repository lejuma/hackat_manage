<?php
include_once "bd.inc.php";

if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}
require_once("$root/model/Hackathon.php");
require_once("$root/model/Member.php");

function getHackathons() : array {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from hackathon");
        $req->execute();

        $req->setFetchMode(PDO::FETCH_CLASS,'Hackathon');
        while ($hackathon = $req->fetch()) {
            $resultat[] = $hackathon;
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getHackathon(int $id) : Hackathon {
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from hackathon where id = :id");
        $req->bindParam(':id',$id);
        $req->execute();

        $req->setFetchMode(PDO::FETCH_CLASS,'Hackathon');
        $hackathon = $req->fetch();

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $hackathon;
}

function insertInscription(int $idHackathon, string $prenom, string $nom)
{
    try {
        $cnx = connexionPDO();
        $req1 = $cnx->prepare("insert into member(firstname, lastname) values (:firstname,:lastname)");
        $req1->bindParam(":firstname",$prenom,PDO::PARAM_STR);
        $req1->bindParam(":lastname",$nom,PDO::PARAM_STR);
        $req1->execute();
        $idMembre = $cnx->lastInsertId();
        $req2 = $cnx->prepare("insert into participation(hackathonid, memberid, roleid) values (:hackathonid,:memberid, 2)");
        $req2->bindParam(':hackathonid',$idHackathon, PDO::PARAM_INT);
        $req2->bindParam(':memberid',$idMembre, PDO::PARAM_INT);
        $req2->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

function getInscriptions(int $idHackathon) : ?array {
    $inscrits= array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select m.* from member m inner join participation p on m.id = p.memberid  where p.hackathonid = :idhackathon and p.roleid = 2");
        $req->bindParam(':idhackathon',$idHackathon, PDO::PARAM_INT);
        $req->execute();

        $req->setFetchMode(PDO::FETCH_CLASS,'Member');
        while ($member = $req->fetch()) {
            $inscrits[] = $member;
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $inscrits;
}

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    // prog principal de test
    header('Content-Type:text/plain');

    echo "getHackathons() : \n";
    print_r(getHackathons());

    echo "getHackathon(1) : \n";
    print_r(getHackathon(1));

    echo "insertInscription(2,\"Miles\", \"Davis\")\n";
    insertInscription(2,"Miles", "Davis");

    echo "getInscriptions(2) : \n";
    print_r(getInscriptions(2));

}