<?php include "$root/view/header.html.php"; ?>


    <h1><?php print($hackathon->getName()) ?></h1>
    <table>
        <?php
        print('<tr><td>Topic : </td><td>' . $hackathon->getTopic() . '</td></tr>');
        print('<tr><td>Description : </td><td>' . $hackathon->getDescription() . '</td></tr>');
        ?>
    </table>
<br>
    <b>Liste des inscrits :</b>
<br><br>
    <table class="bordertable">
        <tr>
            <th>Prénom</th>
            <th>Nom</th>
        </tr>
        <?php foreach($inscriptions as $r)
        {
            print("<tr><td>" . $r->getFirstname() . "</td>" . "<td>" . $r->getLastname() . "</td></tr>");
        }
        ?>
    </table>
<br>
    <a href="/?object=hackathon&action=inscription&id=<?php print ($hackathon->getId()); ?>">Nouvelle inscription</a>

<?php include "$root/view/footer.html.php";
