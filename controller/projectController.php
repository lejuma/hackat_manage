<?php
if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}

require_once("$root/model/Project.php");
require_once("$root/dal/bd.project.inc.php");

// creation du menu burger
$burgerMenu = array();
$burgerMenu[] = Array("url"=>"./index.php?object=project&action=all","label"=>"Tous");

// recuperation de l'action
if (isset($_GET["action"])){
    $action = $_GET["action"];
}
else {
    $action = "all";
}

// Gestion des différentes fonctionalités
switch($action) {

    case 'all':
        // Affichage de la liste des projets

        // 1 - Recuperation de la liste à partir de la BDD

        $projectList = getProjects();

        // 2 - Affichage de la liste

        include "$root/view/project/allProject.html.php";

        break;

    case 'details':

        // TODO A faire

        // 1 - Recuperation des infos du projet

        $idProject = $_GET['id'];

        //...

        // 2 - Affichage des infos du projet

        include "$root/view/project/detailsProject.html.php";

        break;

    default:
        include "$root/view/error/400.html.php";

}